module.exports = {
    env: {
        db_apiKey: process.env.db_apiKey,
        db_authDomain: process.env.db_authDomain,
        db_databaseURL: process.env.db_databaseURL,
        db_projectId: process.env.db_databaseURL,
        db_storageBucket: process.env.db_storageBucket,
        db_messagingSenderId: process.env.db_messagingSenderId,
        db_appId: process.env.db_appId,
        db_measurementId: process.env.db_measurementId,
        firebase_client_email: process.env.firebase_client_email,
        firebase_private_key:process.env.firebase_private_key
    },
    webpack: (config, { isServer }) => {
        // Fixes npm packages that depend on `fs` module
        if (!isServer) {
          config.node = {
            fs: 'empty'
          }
        }
    
        return config
      }
  }