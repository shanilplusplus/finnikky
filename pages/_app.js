import {wrapper} from '../src/store/configureStore';
import '../styles/globals.scss';
import 'react-dates/lib/css/_datepicker.css';
import 'react-pure-modal/dist/react-pure-modal.min.css';
import { ChakraProvider } from "@chakra-ui/react"


function MyApp({ Component, pageProps }) {
  
  return(
    <>
      {/* <Head>
        <title>Finnikky</title>
        <link rel="icon" href="/favicon.ico" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" /> 
      </Head> */}
    <ChakraProvider  resetCSS={false}>
      <Component {...pageProps} />
      </ChakraProvider>

    </>
  )
}

export default wrapper.withRedux(MyApp);

