import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
static async getInitialProps(ctx) {
const initialProps = await Document.getInitialProps(ctx)
return { ...initialProps }
}

render() {
return (
    <Html lang="en">
    <Head>
        <title>Finnikky</title>
        <link rel="icon" href="/favicon.ico" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" /> 
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
    </Head>
    <body>
        <Main />
        <NextScript />
    </body>
    </Html>
)
}
}

export default MyDocument