import db from '../src/firebase/firebase';
import { connect } from 'react-redux';
import jwt from 'jsonwebtoken'; 
import cookie from 'cookie';
import Transactions from '../src/components/Transactions';
import Navigation from '../src/components/Navigation';
import withAuth from '../src/utils/withAuth';


const TransactionsPage = (props) => {

  let sortedTlist = props.transactions.reverse();

  
    return(
        <div className="outer-container">
          <Navigation />
          <div className="transaction-page-content">
                
              <Transactions TList={sortedTlist}/>
          </div>
            
        </div>
    )
}

export const getServerSideProps = async(context) => {

  let decoded = '';
  if(context.req.headers.cookie){
    const parsedCookies = cookie.parse(context.req.headers.cookie)
    decoded = jwt.decode(parsedCookies.userId, { header: true })
  }
  
  
 
  let listOfTransactions = [];
   const dbreq = await db.ref(`users/${decoded}/transactions`)
  .once('value')
      .then((snapshot) => snapshot.val())
      .then((val) => {
        Object.keys(val).map((key) => {
          listOfTransactions.push({
            id: key,
            ...val[key]
          })
        }
        
        );

  })
      .catch((e) => {
      console.log('error fetching data', e)
  })

  return {
    props : {
      transactions: [...listOfTransactions]
    }
  }
}





const mapStateToProps = () => {
    return {
       
    };
  };

  
  // export default connect(mapStateToProps)(TransactionsPage);
  export default connect(mapStateToProps)(withAuth(TransactionsPage));






