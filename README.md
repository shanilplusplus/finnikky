# Finnikky

A personal financial application for tracking and budgeting expenses.

### Prerequisites

you need to have node installed in your computer.

```
npm install webpack -g
```

## Getting Started

to get started on your local machine, clone this repo, and install all the node modules. 


### Installing
cd into the folder
install npm dependencies


### Running Locally

```
npm run decv
```

### View Demo
[Finnikky](https://www.finnikky.com/)

### Demo Account

```
email: hi@sigera.ca 
password:  111qqq

```

## Built With

* [React.js](https://reactjs.org/) - for React and ReactDom libraries
* [next.js](https://nextjs.org/) - bundling
* [babel](https://rometools.github.io/rome/) - compiling and transpling
* [Firebase](https://rometools.github.io/rome/) - Database
* [sass](https://rometools.github.io/rome/) - for scss


## Authors

* **Shaun Sigera** (http://sigera.ca/)


## License

This project is licensed under the MIT License


